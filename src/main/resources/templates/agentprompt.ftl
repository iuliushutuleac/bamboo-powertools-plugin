[#-- @ftlvariable name="" type="com.valens.bamboo.tools.AgentConsoleAction" --]
[#-- @ftlvariable name="action" type="com.valens.bamboo.tools.AgentConsoleAction" --]

<html>
    <head>
        <title>Agent Console</title>
        <meta name="tab" content="agentConsole"/>
        <meta name="decorator" content="result"/>
    </head>

    <body>
        <h1>Agent Console</h1>
        <div class="agConsoleParam">
            [@ww.form action='generateAgentConsole' submitLabelKey='global.buttons.submit' cssClass='bambooForm'
            method='view' ]
            [@ww.textfield name="workingFolder" label="Working Folder"  cssClass="full-width-field" /]        
            [@ww.textfield name="command" label="Command"   cssClass="full-width-field"/]
            
            <textarea rows="20" class="aui full-width-field" style="margin-top: 0px; height: 328px; width: 100%; margin-left: 0px;" readonly='true'>${executionResult}</textarea>
            [@ww.hidden name='planName' value=planName /]
            [@ww.hidden name='planKey' value=planKey /]
            [@ww.hidden name='buildKey' value=buildKey /]
            [@ww.hidden name='buildNumber' value=buildNumber /]
            [@ww.hidden name='baseUrl' value=baseUrl /]  
            [/@ww.form]

        </div>
    </body>
</html>