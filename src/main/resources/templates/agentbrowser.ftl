[#-- @ftlvariable name="" type="com.valens.bamboo.tools.AgentBrowser" --]
[#-- @ftlvariable name="action" type="com.valens.bamboo.tools.AgentBrowser" --]

<html>
    <head>
        <title>Agent browser</title>
        <meta name="tab" content="agentBrowser"/>
        <meta name="decorator" content="result"/>
    </head>

    <body>
        <h1>Agent browser</h1>
        <div class="agParam">
            [@ww.form action='generateAgentBrowser' submitLabelKey='global.buttons.submit' cssClass='bambooForm'
            method='view' ]
            [@ww.textfield name="workingFolder" label="Working Folder" readonly='true' cssClass="full-width-field" /]        
            [@ww.textfield name="path" label="Path" readonly='true'  cssClass="full-width-field"/]
            <div class="fileListDiv">
                <table class="aui">
                    <tr><th>Name</th><th>Type</th><th>Attributes</th></tr>
                    [#list fileList as file]                    
                    [#if file.content??]
                    <tr>
                        <td colspan="3">
                            <textarea rows="20" class="aui full-width-field" style="margin-top: 0px; height: 328px; width: 100%; margin-left: 0px;" readonly='true'>${file.content}</textarea>
                        </td>
                    </tr>
                    [#else]
                    <tr>
                        <td><a id="navFile_${file.getName()}" href="${req.contextPath}/build/viewAgentBrowser.action?buildKey=${buildKey}&amp;buildNumber=${buildNumber}&amp;path=${path}/${file.getName()}">${file.getName()}</a></td>
                        <td>${file.type}</td>
                        <td>${file.attributes}</td>
                    </tr>
                    [/#if]
                    [/#list]
                </table>
            </div>

            [@ww.hidden name='planName' value=planName /]
            [@ww.hidden name='planKey' value=planKey /]
            [@ww.hidden name='buildKey' value=buildKey /]
            [@ww.hidden name='buildNumber' value=buildNumber /]
            [@ww.hidden name='baseUrl' value=baseUrl /]  
            [/@ww.form]

        </div>
    </body>
</html>