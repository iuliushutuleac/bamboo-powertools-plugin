/* 
 * Copyright (C) 2016 IHutuleac
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.valens.bamboo.tools.entities;

import java.io.Serializable;

/**
 *
 * @author IHutuleac
 */
public class BasicFileInfo  implements Serializable
{
    private String name;
    private String type;
    private String attributes;
    private String content;

    public BasicFileInfo(String name, String type, String attributes, String content)
    {
        this.name = name;
        this.type = type;
        this.attributes = attributes;
        this.content = content;
    }

    public BasicFileInfo(String name, String type, String attributes)
    {
        this.name = name;
        this.type = type;
        this.attributes = attributes;
    }

    public BasicFileInfo(String content)
    {
        this.content = content;
    }

    public BasicFileInfo()
    {
    }
    
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getAttributes()
    {
        return attributes;
    }

    public void setAttributes(String attributes)
    {
        this.attributes = attributes;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(String content)
    {
        this.content = content;
    }

    @Override
    public String toString()
    {
        return "BasicFileInfo{" + "name=" + name + ", type=" + type + ", attributes=" + attributes + ", content=" + content + '}';
    }
    
    
}
