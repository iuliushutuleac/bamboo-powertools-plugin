/* 
 * Copyright (C) 2016 IHutuleac
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.valens.bamboo.tools.messagetypes;

import com.atlassian.bamboo.bandana.PlanAwareBandanaContext;
import com.atlassian.bamboo.process.CommandlineStringUtils;
import com.atlassian.bamboo.process.ExternalProcessBuilder;
import com.atlassian.bamboo.process.ProcessService;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.v2.build.agent.messages.AbstractBambooAgentMessage;
import com.atlassian.bamboo.v2.build.agent.messages.RemoteBambooMessage;
import com.atlassian.bandana.BandanaManager;
import com.atlassian.spring.container.ContainerManager;
import com.atlassian.utils.process.ExternalProcess;
import com.google.common.collect.ImmutableList;
import com.valens.bamboo.tools.Constants;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang.StringEscapeUtils;

/**
 *
 * @author IHutuleac
 */
public class CommandMessage extends AbstractBambooAgentMessage implements RemoteBambooMessage
{

    private String command;
    private String workingFolder;

    Long id;
    private ProcessService processService;

    public String getWorkingFolder()
    {
        return workingFolder;
    }

    public void setWorkingFolder(String workingFolder)
    {
        this.workingFolder = workingFolder;
    }

    public CommandMessage(String command, Long id)
    {
        this.command = command;
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getCommand()
    {
        return command;
    }

    public void setCommand(String command)
    {
        this.command = command;
    }

    public Object deliver()
    {
        final List<String> arguments = CommandlineStringUtils.tokeniseCommandline(command);
        final ImmutableList.Builder<String> commandListBuilder = ImmutableList.<String>builder();
        BandanaManager bandanaManager = (BandanaManager) ContainerManager.getComponent("bandanaManager");

        commandListBuilder.addAll(arguments);
        Runtime runtime = Runtime.getRuntime();
        Process process;
        Object result = null;
        try
        {

            process = runtime.exec(command, null, new File(getWorkingFolder()));

            InputStream inStream = process.getInputStream();
            InputStreamReader sReader = new InputStreamReader(inStream);
            BufferedReader bufferedReader = new BufferedReader(sReader);
            String s = "";
            String line;
            int k = 0;
            while ((line = bufferedReader.readLine()) != null && k < 1000)
            {
                k++;
                s = s + line + "\n";
            }
            result = StringEscapeUtils.escapeHtml(s);
            try
            {
                bandanaManager.setValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, Constants.REPLY_MESSAGE_PREFIX + ":" + id, StringEscapeUtils.escapeHtml(s));
            } catch (Exception e)
            {

            }
        } catch (IOException ex)
        {
            result = StringEscapeUtils.escapeHtml("An error occured " + ex.getMessage());
            try
            {
                bandanaManager.setValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, Constants.REPLY_MESSAGE_PREFIX + ":" + id, StringEscapeUtils.escapeHtml("An error occured " + ex.getMessage()));
            } catch (Exception e)
            {

            }
            Logger.getLogger(CommandMessage.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

}
