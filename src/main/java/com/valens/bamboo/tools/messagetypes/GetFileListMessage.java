/* 
 * Copyright (C) 2016 IHutuleac
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.valens.bamboo.tools.messagetypes;

import com.atlassian.bamboo.bandana.PlanAwareBandanaContext;
import com.atlassian.bamboo.v2.build.agent.messages.AbstractBambooAgentMessage;
import com.atlassian.bamboo.v2.build.agent.messages.RemoteBambooMessage;
import com.atlassian.bandana.BandanaManager;
import com.atlassian.spring.container.ContainerManager;
import com.valens.bamboo.tools.Constants;
import com.valens.bamboo.tools.entities.BasicFileInfo;
import java.io.File;
import java.io.IOException;
import org.apache.log4j.Logger;

import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.logging.Level;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringEscapeUtils;

public class GetFileListMessage extends AbstractBambooAgentMessage implements RemoteBambooMessage
{

    private static final Logger log = Logger.getLogger(GetFileListMessage.class);

    String content;
    Long id;

    public String getContent()
    {
        return content;
    }

    public void setContent(String content)
    {
        this.content = content;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public GetFileListMessage(String content, Long id)
    {
        super();
        this.content = content;
        this.id = id;
    }

    @Override
    public Object deliver()
    {

        BandanaManager bandanaManager = (BandanaManager) ContainerManager.getComponent("bandanaManager");

        String pid = ManagementFactory.getRuntimeMXBean().getName();

        ArrayList<BasicFileInfo> arr = new ArrayList<BasicFileInfo>();
        BasicFileInfo b = null;

        File f = new File(((String) content));
        if (f.exists())
        {
            if (f.isDirectory())
            {
                for (File au : f.listFiles())
                {
                    if (au.isDirectory())
                    {
                        arr.add(new BasicFileInfo(au.getName(), (au.isDirectory() ? "DIR" : "FILE"), ""));
                    } else
                    {
                        arr.add(new BasicFileInfo(au.getName(), (au.isDirectory() ? "DIR" : "FILE"), "" + au.length()));
                    }
                }

            } else
            {
                b = new BasicFileInfo(f.getName(), (f.isDirectory() ? "DIR" : "FILE"), "" + f.length());

                if (f.length() < 50000)
                {
                    try
                    {
                        b.setContent(StringEscapeUtils.escapeHtml(FileUtils.readFileToString(f)));
                    } catch (IOException ex)
                    {
                        b.setContent(StringEscapeUtils.escapeHtml("Some error occured: " + ex.getMessage()));
                        java.util.logging.Logger.getLogger(GetFileListMessage.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else
                {
                    b.setContent(StringEscapeUtils.escapeHtml("File too large for live view ..."));
                }
                arr.add(b);
            }
        } else
        {
            b = new BasicFileInfo(f.getName(), "FILE", "0");
            b.setContent(StringEscapeUtils.escapeHtml("File not found ... " + (String) content));
            arr.add(b);
        }

        try
        {
            bandanaManager.setValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, Constants.REPLY_MESSAGE_PREFIX + ":" + id, arr);
        } catch (Exception e)
        {   
            e.printStackTrace();
        }
        log.info("Received content: " + content);
        log.info("Reply content: " + Constants.REPLY_MESSAGE_PREFIX + ":" + id + " : " + arr.toString());
        return arr;

    }

}
