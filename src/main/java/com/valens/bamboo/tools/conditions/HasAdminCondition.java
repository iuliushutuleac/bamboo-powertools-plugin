/* 
 * Copyright (C) 2016 IHutuleac
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.valens.bamboo.tools.conditions;

import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.plugins.web.conditions.ConditionHelper;
import com.atlassian.bamboo.security.BambooPermissionManager;
import com.atlassian.bamboo.security.acegi.acls.BambooPermission;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;
import org.acegisecurity.acls.Permission;
import org.apache.log4j.Logger;

import java.util.Map;
import java.util.Optional;

@SuppressWarnings("unused")
public class HasAdminCondition implements Condition
{
    private static final Logger log = Logger.getLogger(HasAdminCondition.class);

    protected BambooPermissionManager bambooPermissionManager;

    @Override
    public void init(final Map<String, String> params) throws PluginParseException {
    }

    public HasAdminCondition(final BambooPermissionManager bambooPermissionManager)
    {
        this.bambooPermissionManager = bambooPermissionManager;
    }

    boolean checkPlanPermission(Map<String, Object> context, Permission permission) {
        Optional<PlanKey> planKey = ConditionHelper.getPlanKey(context);
        return planKey.isPresent() && this.bambooPermissionManager.hasPlanPermission(permission, (PlanKey)planKey.get());
    }

    @Override
    public boolean shouldDisplay(Map<String, Object> context)
    {
        return checkPlanPermission(context, BambooPermission.WRITE);
    }

}
