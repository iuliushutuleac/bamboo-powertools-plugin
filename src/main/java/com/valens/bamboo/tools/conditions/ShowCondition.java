/* 
 * Copyright (C) 2016 IHutuleac
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.valens.bamboo.tools.conditions;


import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.cache.CachedPlanManager;
import com.atlassian.bamboo.plan.cache.ImmutablePlan;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;

import java.util.Map;
@SuppressWarnings("unused")
public class ShowCondition implements Condition
{

    private CachedPlanManager cachedPlanManager;

    public CachedPlanManager getCachedPlanManager() {
        return cachedPlanManager;
    }

    /**
     * Called after creation and autowiring.
     *
     * @param params The optional map of parameters specified in XML.
     */
    @Override
    public void init(Map params) throws PluginParseException
    {
        // Do nothing
    }

    /**
     * Determine whether the web fragment should be displayed
     *
     * @return true if the user should see the fragment, false otherwise
     */
    @Override
    public boolean shouldDisplay(Map<String,Object> context)
    {
        final String buildKey = (String) context.get("buildKey");

        if (buildKey != null)
        {
            final ImmutablePlan plan = cachedPlanManager.getPlanByKey(PlanKeys.getPlanKey(buildKey));
            if (plan != null)
            {
                return true;
            }
        }
        return false;
    }

    public void setCachedPlanManager(CachedPlanManager cachedPlanManager)
    {
        this.cachedPlanManager = cachedPlanManager;
    }
}