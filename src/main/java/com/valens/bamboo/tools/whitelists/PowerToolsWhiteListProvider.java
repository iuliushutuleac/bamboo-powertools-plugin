/* 
 * Copyright (C) 2016 IHutuleac
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.valens.bamboo.tools.whitelists;

import com.atlassian.bamboo.security.SerializableClassWhitelistProvider;
import com.valens.bamboo.tools.messagetypes.GetFileListMessage;
import com.valens.bamboo.tools.entities.BasicFileInfo;
import com.valens.bamboo.tools.messagetypes.CommandMessage;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;

public class PowerToolsWhiteListProvider implements SerializableClassWhitelistProvider
{
    private static final List<String> WHITELISTED_CLASSES = Arrays.asList(GetFileListMessage.class.getName(),
            BasicFileInfo.class.getName(),
            CommandMessage.class.getName()
    );

    @NotNull
    @Override
    public Iterable<String> getWhitelistedClasses()
    {
        return WHITELISTED_CLASSES;
    }
}