/* 
 * Copyright (C) 2016 IHutuleac
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.valens.bamboo.tools;

import com.atlassian.bamboo.bandana.PlanAwareBandanaContext;
import com.atlassian.bamboo.util.RequestCacheThreadLocal;
import com.atlassian.bamboo.v2.build.agent.AgentCommandSender;
import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import com.atlassian.bamboo.v2.build.agent.LocalBuildAgent;
import com.atlassian.bamboo.v2.build.agent.messages.RemoteBambooMessage;
import com.atlassian.bandana.BandanaManager;
import com.atlassian.spring.container.ContainerManager;
import com.valens.bamboo.tools.actions.AgentBrowserAction;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.logging.Level;

/**
 *
 * @author IHutuleac
 */


public class MessagingComponent
{

    private AgentCommandSender agentCommandSender;
    BandanaManager bandanaManager;

    public static Logger getLog() {
        return log;
    }

    private static final Logger log = Logger.getLogger(AgentBrowserAction.class);

    public Object runCommand(Long id, final RemoteBambooMessage msg, final BuildAgent buildAgent)
    {
        final HashMap<BuildAgent, Object > results = new HashMap<BuildAgent, Object >();
        if (buildAgent != null)
            
        {
            
            buildAgent.accept(new BuildAgent.BuildAgentVisitor()
            {
                @Override
                public void visitLocal(final LocalBuildAgent localBuildAgent)
                {
                    //TODO: check local agents code
                    log.info("sending command to local agent");
                    //getAgentCommandSender().send(msg, buildAgent.getId());
                    results.put(buildAgent, msg.deliver());
                }

                @Override
                public void visitRemote(final BuildAgent remoteBuildAgent)
                {
                    log.info("sending command to remote agent");

                    getAgentCommandSender().send(msg, buildAgent.getId());
                }
            });
        } else
        {
            log.warn("Sorry but I couldn't get a build agent with the id: " + buildAgent.getId());
        }
        
        if(results.get(buildAgent) != null)
            return results.get(buildAgent);
        
        int k = 0;
        while (k<300)
        {
            k ++;
            try
            {
                Thread.sleep(300);
            } catch (InterruptedException ex)
            {
                java.util.logging.Logger.getLogger(MessagingComponent.class.getName()).log(Level.SEVERE, null, ex);
            }

            for (String key : getBandanaManager().getKeys(PlanAwareBandanaContext.GLOBAL_CONTEXT))
            {
                if (key.startsWith(Constants.REPLY_MESSAGE_PREFIX))
                {
                    
                    long timestamp = Long.parseLong(key.split(":")[1]);
                    if (System.currentTimeMillis() - timestamp > 10000)
                    {

                        getBandanaManager().removeValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, key);
                    }
                    if (timestamp == id)
                    {
                        k = 999;
                        Object o = getBandanaManager().getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, key);
                        RequestCacheThreadLocal.getRequestCache().put("bamboo.http.request.isMutative",Boolean.TRUE);
                        getBandanaManager().removeValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, key);
                        return o;
                    }

                }

            }
        }
        return "";
    }

//    public void reply(ReplyMessage obj)
//    {
//        
//        replies.put(obj.getCommandId(), obj);
//        for(Long r : replies.keySet())
//        {
//            if(System.currentTimeMillis() - r > 100000 )
//                replies.remove(r);
//        }
//    }
    public AgentCommandSender getAgentCommandSender()
    {
        if (this.agentCommandSender == null)
        {
            this.agentCommandSender = (AgentCommandSender) ContainerManager.getComponent("agentCommandSender");
        }

        return agentCommandSender;
    }

    public void setAgentCommandSender(AgentCommandSender agentCommandSender)
    {

        this.agentCommandSender = agentCommandSender;
    }

    public BandanaManager getBandanaManager()
    {
        if (this.bandanaManager == null)
        {
            this.bandanaManager = (BandanaManager) ContainerManager.getComponent("bandanaManager");
        }
        return bandanaManager;
    }

    public void setBandanaManager(BandanaManager bandanaManager)
    {
        this.bandanaManager = bandanaManager;
    }

}
