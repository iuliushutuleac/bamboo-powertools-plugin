/* 
 * Copyright (C) 2016 IHutuleac
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.valens.bamboo.tools.actions;

import com.atlassian.bamboo.build.BuildResultsAction;
import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import com.valens.bamboo.tools.MessagingComponent;
import com.valens.bamboo.tools.messagetypes.CommandMessage;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.Nullable;

public class AgentConsoleAction extends BuildResultsAction
{

    private MessagingComponent messagingComponent;
    private String workingFolder;
    private String command;

    public MessagingComponent getMessagingComponent() {
        return messagingComponent;
    }

    public void setMessagingComponent(MessagingComponent messagingComponent) {
        this.messagingComponent = messagingComponent;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    private BuildAgent getAgent(@Nullable Long agentId) {
        return agentId != null ? this.agentManager.getAgent(agentId.longValue()) : null;
    }

    private static final Logger log = Logger.getLogger(AgentConsoleAction.class);

    public String getWorkingFolder()
    {
        if(workingFolder == null)
            workingFolder = this.getMaskedMetadata().get("build.working.directory");
        return workingFolder;
    }

    @Override
    public String doExecute() throws Exception
    {
        super.doExecute();
        return INPUT;
    }

    public String getExecutionResult()
    {
        try
        {
            if (this.getBuildResultsSummary() != null)
            {
                final BuildAgent buildAgent = this.getAgent(this.getBuildResultsSummary().getBuildAgentId());
                if (getCommand().trim().length() > 0)
                {
                    CommandMessage ms = new CommandMessage(command, System.currentTimeMillis());
                    ms.setWorkingFolder(workingFolder);
                    return (String) messagingComponent.runCommand(ms.getId(), ms, buildAgent);
                }

            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        return "";
    }

}
