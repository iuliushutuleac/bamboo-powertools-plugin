/* 
 * Copyright (C) 2016 IHutuleac
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.valens.bamboo.tools.actions;

import com.atlassian.bamboo.build.PlanResultsAction;
import com.atlassian.bamboo.security.BambooPermissionManager;
import com.atlassian.bamboo.v2.build.agent.AgentCommandSender;
import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import com.valens.bamboo.tools.MessagingComponent;
import com.valens.bamboo.tools.entities.BasicFileInfo;
import com.valens.bamboo.tools.messagetypes.GetFileListMessage;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

public class AgentBrowserAction extends PlanResultsAction
{

    private AgentCommandSender agentCommandSender;
    private MessagingComponent messagingComponent;

    public AgentCommandSender getAgentCommandSender() {
        return agentCommandSender;
    }

    public void setAgentCommandSender(AgentCommandSender agentCommandSender) {
        this.agentCommandSender = agentCommandSender;
    }

    public MessagingComponent getMessagingComponent() {
        return messagingComponent;
    }

    public void setMessagingComponent(MessagingComponent messagingComponent) {
        this.messagingComponent = messagingComponent;
    }

    private String path = "/";
    private String workingFolder = "/";
    private String completePath = "/";

    private static final Logger log = Logger.getLogger(AgentBrowserAction.class);

    @Override
    public String doExecute() throws Exception
    {
        String result = super.doExecute();
        return INPUT;
    }

    public String doChangeDir() throws Exception
    {
        String result = super.doExecute();
        return INPUT;
    }

    public String getPath()
    {
        return path;
    }

    public void setPath(String path)
    {
        if(path.endsWith(".."))
        {
            try{
                path=new File(path).getParent();
                path=new File(path).getParent();
            }catch(Exception e)
            {
                path="/";
            }
            
        }
        path = path.replace("//", "/");
        this.path = path;
    }

    public String getWorkingFolder()
    {
        workingFolder = this.getMaskedMetadata().get("build.working.directory");
        return workingFolder;
    }

    public void setWorkingFolder(String s)
    {
        path = "/";
        workingFolder = this.getMaskedMetadata().get("build.working.directory");
    }

    public String getCompletePath()
    {        
        completePath = this.getMaskedMetadata().get("build.working.directory") + path;
        
        try
        {
            log.warn("Verify security complete path: " + new File(completePath).getCanonicalPath().toString() 
                    + " against working folder " + new File(workingFolder).getCanonicalPath().toString());
        
            if(new File(completePath).getCanonicalPath().toString().startsWith(new File(workingFolder).getCanonicalPath().toString()))                
                log.warn("Allowing ...");
            if(new File(completePath).getCanonicalPath().toString().startsWith(new File(workingFolder).getCanonicalPath().toString()))                
                return completePath;
            else
            {
                path = "/";
                return workingFolder;
            }
        } catch (IOException ex)
        {
            path = "/";
            return workingFolder;
        }
    }

    private BuildAgent getAgent(@Nullable Long agentId) {
        return agentId != null ? this.agentManager.getAgent(agentId.longValue()) : null;
    }

    public void setCompletePath(String s)
    {
        completePath = s;
    }

    public Collection<BasicFileInfo> getFileList()
    {
        SecurityContextHolder.getContext().setAuthentication(BambooPermissionManager.SYSTEM_AUTHORITY);
        try
        {
            if (getResultsSummary() != null)
            {
                final BuildAgent buildAgent = this.getAgent(this.getResultsSummary().getBuildAgentId());

                GetFileListMessage ms = new GetFileListMessage(getCompletePath(), System.currentTimeMillis());
                Object o = messagingComponent.runCommand(ms.getId(), ms, buildAgent);
                System.out.println(o.toString());
                ArrayList<BasicFileInfo> arr = (ArrayList<BasicFileInfo>) (o);
                arr.add(new BasicFileInfo("..","DIR",""));
                Collections.sort(arr, new Comparator<BasicFileInfo>()
                {
                    public int compare(BasicFileInfo o1, BasicFileInfo o2)
                    {
                        if (o1.getType().equals(o2.getType()))
                        {
                            return (o1.getName().compareTo(o2.getName()));
                        }
                        return (o1.getType().compareTo(o2.getType()));
                    }
                });
                return arr;

            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        return new ArrayList<BasicFileInfo>();
    }

}
