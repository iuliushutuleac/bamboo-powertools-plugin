set M2HOME=D:\Applications\Atlassian\atlassian-plugin-sdk\apache-maven-3.5.4
set PATH=%M2HOME%\bin;%PATH%
set JAVA_HOME=D:\Applications\Java\jdk1.8.0_101

cmd /c mvn build-helper:parse-version versions:set -DnewVersion=${parsedVersion.majorVersion}.${parsedVersion.minorVersion}.${parsedVersion.nextIncrementalVersion}

cmd /c mvn -Dbamboo.version=7.2.1 clean package


git add .

git commit -m "new version"

git push

pause